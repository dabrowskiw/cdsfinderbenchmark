import sys

mgpresfile = open(sys.argv[1], "r")
mgpresults = [x.strip() for x in mgpresfile.read().split(">")[1:]]
mgpresfile.close()

fastafile = open(sys.argv[2], "r")
headers = [x.split("\n")[0] for x in fastafile.read().split(">")[1:]]
fastafile.close()

for mgpres in mgpresults:
    data = [x.strip() for x in mgpres.split(",")]
    header = headers[int(int(data[0])/2)]
    if len(data) == 7:
        cdsstart = data[3]
        cdsend = data[4]
        strand = data[1].replace("'", "")
        frame = int(data[5])+1
    elif len(data) == 6:
        continue
        cdsstart = data[2]
        cdsend = data[3]
        strand = data[1].replace("'", "")
        frame = int(data[4])+1
    else:
        raise "Unknown line format for line '" + mgpres + "' - neither 6 nor 7 columns"
    if strand == "-":
#        frame -= 3
        frame = 5-frame # Assuming that the frame is being counted "backwards" in reverse-strand, since this yields slightly better results for CNN-MGP
    print(">" + header)
    print(cdsstart + "\t" + cdsend + "\t" + strand + "\t" + str(frame) + "\t0.0\tI:\tD:\t")
