import sys

numfiles = int(sys.argv[2])
prefix = sys.argv[3]

outfiles = [open(prefix + str(x) + ".fasta", "w") for x in range(0, numfiles)]

filenum = 0
f = open(sys.argv[1], "r")
seq = []
for line in f:
    if line[0] == ">":
        if len(seq) != 0:
            outfiles[filenum].write("".join(seq))
        seq = []
        filenum += 1
        if filenum == len(outfiles):
            filenum = 0
    seq += [line]

outfiles[filenum].write("".join(seq))
for of in outfiles:
    of.close()

