# cdsFinderBenchmark

Benchmarks fragGeneScan [1] and CNN-MGP [2] on a dataset containing reads simulated from RefSeq CDS [3].

Requires nextflow and singularity to be installed.

Run using:
```
nextflow run benchmark.nf -profile singularity --outdir benchmark_out
```

This will yield a folder ```benchmark_out```, the tool's results are in ```predictions```, the accuracies are in ```accuracies```.

The accuracies have the following meanings:
 * All correct: Percentace of sequences that the tool found a correct CDS for
 * Nonempty correct: Percentage of sequences that the tool found a correct CDS for, excluding sequences that no prediction was made for from the entire analysis
 * Nonempty correct (strict): As above, but treating sequences that had several different predictions as wrong (instead of correct if one of them was correct, as is done in "Nonempty correct")

[1] Rho M, Tang H, Ye Y. FragGeneScan: predicting genes in short and error-prone reads. Nucleic Acids Res. 2010 Nov;38(20):e191. doi: 10.1093/nar/gkq747. Epub 2010 Aug 30. PMID: 20805240; PMCID: PMC2978382.

[2] Al-Ajlan A, El Allali A. CNN-MGP: Convolutional Neural Networks for Metagenomics Gene Prediction. Interdiscip Sci. 2019 Dec;11(4):628-635. doi: 10.1007/s12539-018-0313-4. Epub 2018 Dec 27. PMID: 30588558; PMCID: PMC6841655.

[3] Voigt, Benjamin, Fischer, Oliver, Krumnow, Christian, Herta, Christian, & Dabrowski, Piotr Wojciech. (2020). Refseq datasets for training frame classification (1.0.0) [Data set]. Zenodo. https://doi.org/10.5281/zenodo.4306248
