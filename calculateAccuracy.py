import re
import sys

class Prediction:
    def __init__(self, name, predictions):
        namedata = name.split("|")
        self.correctframe = int(namedata[-1])
        self.predictedframes = []
        for pred in predictions:
            strand = pred[2]
            frame = int(pred[3])
            frame -= 1
            if strand == "-":
                frame += 3
            elif strand == "+":
                pass
            else:
                raise "Strand " + strand + " unknown."
            self.predictedframes += [frame]

    def contains_correct(self):
        for pred in self.predictedframes:
            if pred == self.correctframe:
                return True
        return False

    def contains_only_correct(self):
        for pred in self.predictedframes:
            if pred != self.correctframe:
                return False
        if len(self.predictedframes) == 0:
            return False
        return True

def parseLines(lines):
    try:
        data = lines.strip().split("\n")
        name = data[0]
        if len(data) == 1:
            return Prediction(name, [])
        predictions = []
        for predline in data[1:]:
            predictions += [re.split("\s+", predline, 4)]
        return Prediction(name, predictions)
    except:
        return None


f = open(sys.argv[1], "r")
predictions = [x for x in [parseLines(y) for y in f.read().split(">")[1:]] if x is not None]
f.close()
predictions_nonempty = [x for x in predictions if len(x.predictedframes) != 0]

print("All correct: " + str(100.0*len([x for x in predictions if x.contains_correct()])/len(predictions)))
print("Nonempty correct: " + str(100.0*len([x for x in predictions_nonempty if x.contains_correct()])/len(predictions_nonempty)))
print("Nonempty correct (strict): " + str(100.0*len([x for x in predictions_nonempty if x.contains_only_correct()])/len(predictions_nonempty)))
