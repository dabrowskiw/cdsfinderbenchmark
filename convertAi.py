import sys

f = open(sys.argv[1], "r")
data = [x.split("\n")[0] for x in f.read().split(">")[1:]]
f.close()

for x in data:
    pdata = x.split("|")
    name = pdata[0]
    frame = pdata[-2]
    print(">" + name + "|X|" + frame)
    strand = "+"
    pred = int(pdata[-1])+1
    if pred > 3:
        pred -= 3
        strand = "-"
    print("0\t300\t" + strand + "\t" + str(pred) + "\t0.0\tI:\tD:")
    
