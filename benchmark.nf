nextflow.enable.dsl=2

params.cpus = -1
params.airesults = null

/* No source code/download available
// reference: https://pubmed.ncbi.nlm.nih.gov/23901840/
process mgc {
}
*/

/* Orphelia is an online-only tool, not evaluated
// reference: https://depot.galaxyproject.org/singularity/fraggenescan%3A1.31--h779adbc_3
process orphelia {
}
*/

/* Download site is offline
// reference: https://pubmed.ncbi.nlm.nih.gov/23735199/
process metaGun {
}
*/

/* Download site and documentation offline
// reference: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1636498/
process metaGene {
  publishDir "${params.outdir}/metaGene"
  container "https://depot.galaxyproject.org/singularity/metagene_annotator%3A1.0--h516909a_3"
  input:
    val test
  output:
    path "*"
  script:
    """
    touch metaGene.txt
    """
}
*/

// reference: https://pubmed.ncbi.nlm.nih.gov/20805240/
process fragGeneScan {
  storeDir "${params.outdir}/predictions/fragGeneScan"
  container "https://depot.galaxyproject.org/singularity/fraggenescan%3A1.31--h779adbc_3"
  cpus params.used_cpus
  input:
    tuple path(fasta), path(traindir), val(trainfile), val(cpus)
  output:
    path "fragGeneScan.${fasta.getSimpleName()}", emit: result
  script:
    """
    FragGeneScan -s ${fasta} -o fragGeneScan.${fasta.getSimpleName()} -w 0 -t ${trainfile} -p ${task.cpus}
    mv fragGeneScan.${fasta.getSimpleName()}.out fragGeneScan.${fasta.getSimpleName()}
    """
}


// reference: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6841655/
process cnnMgp {
  storeDir "${params.outdir}/predictions/cnn-mgp"
  container "https://zenodo.org/record/5513440/files/cnn-mgp.sif"
  input:
    path fasta
  output:
    tuple path("${fasta.getSimpleName()}_out_CNN-MGP_coord.coords"), path("${fasta}")
  script:
    """
    CNN-MGP -S -i ${fasta} -o ${fasta.getSimpleName()}_out -st nucl
    """
}

process convertMgpResults {
  input:
    tuple path(coords), path(infasta)
  output:
    path "CNN-MGP.${infasta.getSimpleName()}"
  script:
    """
    python ${baseDir}/convertCnnMgp.py ${coords} ${infasta} > CNN-MGP.${infasta.getSimpleName()}
    """
}

process convertAIResults {
  input:
    path infile
  output:
    path "AI.${infile.getSimpleName()}"
  script:
    """
    python ${baseDir}/convertAi.py ${infile} > AI.${infile.getSimpleName()}
    """
}

process combineResults {
  publishDir "${params.outdir}/predictions/cnn-mgp"
  input:
    path infiles
  output:
    path "${infiles[0].getSimpleName()}.fasta.out"
  script:
    """
    cat ${infiles} > ${infiles[0].getSimpleName()}.fasta.out
    """
}

process getData {
  storeDir "${params.outdir}/data"
  input:
    val url
  output:
    path "refseq/refseq_ds_all_off-frames_fb_DNA_test.fasta", emit: fasta
  script:
    """
    wget -O data.tar.gz ${url}
    tar -xvzf data.tar.gz
    """
}

process getTrainData {
  storeDir "${params.outdir}/data"
  input:
    val trainfile
  output:
    path "train", emit: traindir
  script:
    """
    wget -O FragGeneScan1.31.tar.gz "https://downloads.sourceforge.net/project/fraggenescan/FragGeneScan1.31.tar.gz?ts=gAAAAABhQwrwYxZuCpIAExYnR5Wds-Lzd6chdu0UCSw_A4Y2J6q84jqwL5MT6ihblQH8VF_UF3wppFJwIlhJwyrrM5HTE8xtWw%3D%3D&r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Ffraggenescan%2Ffiles%2FFragGeneScan1.31.tar.gz%2Fdownload"
    tar -xvzf FragGeneScan1.31.tar.gz
    mv FragGeneScan1.31/train .
    """
}

process calculateAccuracy {
  publishDir "${params.outdir}/accuracies", mode: "copy", overwrite: true
  input:
    path predfile
  output:
    path "${predfile.getSimpleName()}.txt"
  script:
    """
    python ${baseDir}/calculateAccuracy.py ${predfile} > ${predfile.getSimpleName()}.txt
    """
}

process splitData {
  input:
    val splitnum
    path infile
  output:
    path "infile*.fasta"
  script:
    """
    python ${baseDir}/splitFasta.py ${infile} ${splitnum} infile
    """
}

workflow {
  maxcpus = Runtime.runtime.availableProcessors()
  cpus = params.cpus
  if(params.cpus == -1) {
    cpus = maxcpus
  }
  if(cpus > maxcpus) {
    cpus = maxcpus
  }
  params.used_cpus = cpus
  airesultsfile = params.airesultfile
  ai_results = channel.empty()
  if(airesultsfile != null) {
    ai_results = convertAIResults(channel.fromPath(airesultsfile))
  }
  println "Running using ${cpus} CPUs."
  fasta_full_channel = getData(channel.from("https://zenodo.org/record/4306248/files/refseq.tar.gz?download=1"))
  fasta_channel = splitData(channel.from(cpus), fasta_full_channel).flatten()
  trainfile_channel = channel.from("illumina_1")
  train_channel = getTrainData(trainfile_channel)
//  fragGeneScan_inchannel = fasta_channel.combine(train_channel).combine(trainfile_channel)
  fragGeneScan_inchannel = fasta_full_channel.combine(train_channel).combine(trainfile_channel).combine(channel.from(cpus))
  fragGeneScan_results = fragGeneScan(fragGeneScan_inchannel)
  cnnMgp_coords = cnnMgp(fasta_channel)
  cnnMgp_results = convertMgpResults(cnnMgp_coords)
  allresults_channel = cnnMgp_results.collect().concat(fragGeneScan_results.collect()).concat(ai_results.collect())
  collected = combineResults(allresults_channel)
  calculateAccuracy(collected)
}
